package com.tomiche;

import com.tomiche.dictionary.io.DictionaryLoader;
import com.tomiche.exceptions.DictionaryDoesNotExist;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class MotusBotApp {
    public static void main(String[] args) {
        DictionaryLoader dictionaryLoader = new DictionaryLoader();
        try {
            dictionaryLoader.loadDictionaryOfWordsWithThisSpecificNumberOfLetters(4);
        } catch (DictionaryDoesNotExist e) {

            log.error("This dictionary does not exist !");
        }
        var resultList = dictionaryLoader.getLoadedDictionary()
                .getWordsContainingTheseLetters(Arrays.asList('a','b','l'));
        if(resultList.isPresent()){
            for (String result : resultList.get()){
                log.error(result);
            }
        }

        log.error("More precise");
        resultList = dictionaryLoader.getLoadedDictionary()
                .getWordsContainingTheseLettersInThisOrder(Arrays.asList("b","a","l"));
        if(resultList.isPresent()){
            for (String result : resultList.get()){
                log.error(result);
            }
        }
    }
}
