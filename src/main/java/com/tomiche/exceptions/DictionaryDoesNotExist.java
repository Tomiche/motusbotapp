package com.tomiche.exceptions;

public class DictionaryDoesNotExist extends Exception{
    public DictionaryDoesNotExist(){
        super("No Dictionary found.");
    }
}
