package com.tomiche.dictionary;

import java.util.ArrayList;
import java.util.List;

public class FinderUtil {
    private FinderUtil() {

    }

    public static List<String> returnIntersection(List<String> list1, List<String> list2) {
        List<String> list = new ArrayList<>();

        for (String item : list1) {
            if (list2.contains(item)) {
                list.add(item);
            }
        }

        return list;
    }
}
