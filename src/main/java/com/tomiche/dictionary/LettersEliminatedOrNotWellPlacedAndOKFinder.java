package com.tomiche.dictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class LettersEliminatedOrNotWellPlacedAndOKFinder {
    private final OrderedLettersFinder orderedLettersFinder;

    public LettersEliminatedOrNotWellPlacedAndOKFinder(OrderedLettersFinder orderedLettersFinder) {
        this.orderedLettersFinder = orderedLettersFinder;
    }

    public Optional<List<String>> find(List<String> orderedLettersOK, List<String> lettersNotWellPlaced, List<String> lettersNotInTheWord) {
        if (null == orderedLettersOK || null == lettersNotInTheWord || null == lettersNotWellPlaced)
            return Optional.empty();
        Optional<List<String>> wordsMatchingTheOrderedOKLetters = orderedLettersFinder.find(orderedLettersOK);
        if(wordsMatchingTheOrderedOKLetters.isEmpty()) return Optional.empty();
        Optional<List<String>> wordListReducedByLettersNotPresent = reduceListWithLettersNotPresentInTheWord(wordsMatchingTheOrderedOKLetters.get(), lettersNotInTheWord);
        if(wordListReducedByLettersNotPresent.isEmpty()) return Optional.empty();
        return reduceListWithNotWellPlacedLetters(wordListReducedByLettersNotPresent.get(), lettersNotWellPlaced);
    }

    private Optional<List<String>> reduceListWithNotWellPlacedLetters(List<String> wordListReducedByLettersNotPresent, List<String> lettersNotWellPlaced) {
        Optional<List<String>> wordsWithNoNotWellPlacedLettersWellPlaced = reduceListWithWordsHavingTheNotWellPlacedLettersWellPlaced(wordListReducedByLettersNotPresent, lettersNotWellPlaced);
        if (wordsWithNoNotWellPlacedLettersWellPlaced.isEmpty()) return Optional.empty();
        return reduceListWithWordsNotContainingTheNotWellPlacedLetters(wordsWithNoNotWellPlacedLettersWellPlaced.get(), lettersNotWellPlaced);
    }

    private Optional<List<String>> reduceListWithWordsNotContainingTheNotWellPlacedLetters(List<String> wordsWithNoNotWellPlacedLettersWellPlaced, List<String> lettersNotWellPlaced) {
        List<String> wordsContainingAllTheNotWellPlacedLetters = new ArrayList<>();
        for (String wordWithNoNotWellPlacedLettersWellPlaced : wordsWithNoNotWellPlacedLettersWellPlaced) {
            boolean wordDoesNotContainNotWellPlacedLetter = false;

            for (String letterNotWellPlaced : lettersNotWellPlaced) {
                if (!letterNotWellPlaced.equals("*") && !wordWithNoNotWellPlacedLettersWellPlaced.contains(letterNotWellPlaced.toUpperCase(Locale.ROOT))) {
                    wordDoesNotContainNotWellPlacedLetter = true;
                    break;
                }
            }
            if (!wordDoesNotContainNotWellPlacedLetter) {
                wordsContainingAllTheNotWellPlacedLetters.add(wordWithNoNotWellPlacedLettersWellPlaced);
            }
        }
        return wordsContainingAllTheNotWellPlacedLetters.isEmpty() ? Optional.empty() : Optional.of(wordsContainingAllTheNotWellPlacedLetters);
    }

    private Optional<List<String>> reduceListWithWordsHavingTheNotWellPlacedLettersWellPlaced(List<String> wordListReducedByLettersNotPresent, List<String> lettersNotWellPlaced) {
        List<String> wordListReducedByLettersNotWellPlacedWellPlaced = new ArrayList<>();
        for (String word : wordListReducedByLettersNotPresent) {
            boolean wordHasANotWellPLacedLetterWellPlaced = false;
            for (int i = 0; i < lettersNotWellPlaced.size(); i++) {
                String letterNotWellPlaced = lettersNotWellPlaced.get(i);
                if (!letterNotWellPlaced.equals("*") && letterNotWellPlaced.toUpperCase(Locale.ROOT).charAt(0) == (word.charAt(i))) {
                    wordHasANotWellPLacedLetterWellPlaced = true;
                    break;
                }
            }
            if (!wordHasANotWellPLacedLetterWellPlaced) {
                wordListReducedByLettersNotWellPlacedWellPlaced.add(word);
            }
        }
        return wordListReducedByLettersNotWellPlacedWellPlaced.isEmpty() ? Optional.empty() : Optional.of(wordListReducedByLettersNotWellPlacedWellPlaced);
    }

    private Optional<List<String>> reduceListWithLettersNotPresentInTheWord(List<String> wordsMatchingTheOrderedOKLetters, List<String> lettersNotInTheWord) {
        List<String> wordsReallyMatchingTheOrderedOKLettersWithoutAnyOfTheLettersNotInTheWord = new ArrayList<>();
        for (String wordReallyMatchingTheOrderedOKLetters : wordsMatchingTheOrderedOKLetters) {
            boolean containsALetterNotPresentInTheWord = false;
            for (String letterNotInTheWord : lettersNotInTheWord) {
                if (wordReallyMatchingTheOrderedOKLetters.contains(letterNotInTheWord.toUpperCase(Locale.ROOT))) {
                    containsALetterNotPresentInTheWord = true;
                    break;
                }
            }
            if (!containsALetterNotPresentInTheWord) {
                wordsReallyMatchingTheOrderedOKLettersWithoutAnyOfTheLettersNotInTheWord.add(wordReallyMatchingTheOrderedOKLetters);
            }
        }
        return wordsReallyMatchingTheOrderedOKLettersWithoutAnyOfTheLettersNotInTheWord.isEmpty() ? Optional.empty() : Optional.of(wordsReallyMatchingTheOrderedOKLettersWithoutAnyOfTheLettersNotInTheWord);
    }
}
