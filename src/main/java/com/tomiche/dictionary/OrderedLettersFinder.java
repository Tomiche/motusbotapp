package com.tomiche.dictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.tomiche.dictionary.FinderUtil.returnIntersection;

public class OrderedLettersFinder {

    private final List<String> words;
    private final LetterFinder letterFinder;

    public OrderedLettersFinder(List<String> words, LetterFinder letterFinder){
        this.words = words;
        this.letterFinder = letterFinder;
    }

    public Optional<List<String>> find(List<String> orderedLetters) {
        if (null == orderedLetters || orderedLetters.isEmpty())
            return Optional.empty();
        Optional<List<String>> wordsMatchingTheOrderedLetters = extractWordsWithLettersInThisOrder(orderedLetters);
        if (wordsMatchingTheOrderedLetters.isEmpty())
            return Optional.empty();
        return wordsMatchingTheOrderedLetters;
    }

    private Optional<List<String>> extractWordsWithLettersInThisOrder(List<String> orderedLetters) {
        List<String> wordsMatchingTheOrderedLetters = new ArrayList<>();
        for (int i = 0; i < orderedLetters.size(); i++) {
            List<String> wordsMatchingTheOrderedLetter = extractWordsWithThisLetterInThisPosition(orderedLetters.get(i), i);
            if (wordsMatchingTheOrderedLetter.isEmpty()) {
                return Optional.empty();
            }
            boolean thisLetterIsTheFirstOneChecked = wordsMatchingTheOrderedLetters.isEmpty();
            if (thisLetterIsTheFirstOneChecked)
                wordsMatchingTheOrderedLetters = wordsMatchingTheOrderedLetter;
            else
                wordsMatchingTheOrderedLetters = returnIntersection(wordsMatchingTheOrderedLetter, wordsMatchingTheOrderedLetters);
        }
        if (wordsMatchingTheOrderedLetters.isEmpty())
            return Optional.empty();
        return Optional.of(wordsMatchingTheOrderedLetters);
    }

    private List<String> extractWordsWithThisLetterInThisPosition(String letter, int position) {
        char letterToCheck = letter.toUpperCase(Locale.ROOT).charAt(0);
        if (letterToCheck == '*')
            return words;
        Optional<List<String>> wordsContainingThisLetter = letterFinder.find(letter.charAt(0));

        if (wordsContainingThisLetter.isPresent() && !wordsContainingThisLetter.get().isEmpty())
            return reduceListWithOnlyWordsWithThisLetterInThisPosition(wordsContainingThisLetter.get(), letter, position);
        return new ArrayList<>();
    }

    private List<String> reduceListWithOnlyWordsWithThisLetterInThisPosition(List<String> extractWordsWithThisLetterInThisPosition, String letter, int position) {
        char letterToCheck = letter.toUpperCase(Locale.ROOT).charAt(0);

        return extractWordsWithThisLetterInThisPosition.parallelStream()
                .filter(word -> word.charAt(position) == letterToCheck)
                .collect(Collectors.toList());
    }
}
