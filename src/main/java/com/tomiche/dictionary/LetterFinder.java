package com.tomiche.dictionary;

import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

public class LetterFinder {

    private final List<String> words;

    public LetterFinder(List<String> words) {
        this.words = words;
    }

    public Optional<List<String>> find(char letter) {
        boolean letterIsABlankSpace = letter == ' ';
        if (letterIsABlankSpace)
            return Optional.empty();

        return extractWordsContainingThisLetter(letter);
    }

    private Optional<List<String>> extractWordsContainingThisLetter(char letter) {
        List<String> wordsContainingThisLetter = words.parallelStream()
                .filter(word -> word.contains(String.valueOf(letter).toUpperCase(Locale.ROOT)))
                .collect(Collectors.toList());

        if (wordsContainingThisLetter.isEmpty())
            return Optional.empty();
        return Optional.of(wordsContainingThisLetter);
    }


}
