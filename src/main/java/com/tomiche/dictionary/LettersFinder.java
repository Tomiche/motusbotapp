package com.tomiche.dictionary;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.tomiche.dictionary.FinderUtil.returnIntersection;

public class LettersFinder {
    private final LetterFinder letterFinder;

    public LettersFinder(LetterFinder letterFinder){
        this.letterFinder = letterFinder;
    }
    public Optional<List<String>> find(List<Character> listOfLetters) {
        if (null == listOfLetters)
            return Optional.empty();

        return extractWordsContainingTheseLetters(listOfLetters);
    }

    private Optional<List<String>> extractWordsContainingTheseLetters(List<Character> listOfLetters) {
        List<String> wordsMatchingAllTheLetters = new ArrayList<>();
        for (Character letter : listOfLetters) {
            Optional<List<String>> wordsContainingThisLetter = letterFinder.find(letter);
            boolean thereIsAtLeastOneWordContainingThisLetter = wordsContainingThisLetter.isPresent() && !wordsContainingThisLetter.get().isEmpty();
            if (thereIsAtLeastOneWordContainingThisLetter) {
                wordsMatchingAllTheLetters = getWordsThatContainAllTheLetterAlreadyChecked(wordsMatchingAllTheLetters, wordsContainingThisLetter.get());
            } else {
                return Optional.empty();
            }
        }
        if (wordsMatchingAllTheLetters.isEmpty())
            return Optional.empty();
        return Optional.of(wordsMatchingAllTheLetters);
    }

    private List<String> getWordsThatContainAllTheLetterAlreadyChecked(List<String> currentWordsMatchingAllTheLetters, List<String> wordsContainingThisLetter) {
        boolean thisLetterIsTheFirstOneChecked = currentWordsMatchingAllTheLetters.isEmpty();
        List<String> newWordsMatchingAllTheLetters = new ArrayList<>();
        if (thisLetterIsTheFirstOneChecked) {
            newWordsMatchingAllTheLetters.addAll(wordsContainingThisLetter);
        } else {
            newWordsMatchingAllTheLetters = returnIntersection(currentWordsMatchingAllTheLetters, wordsContainingThisLetter);
        }
        return newWordsMatchingAllTheLetters;
    }


}
