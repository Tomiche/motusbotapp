package com.tomiche.dictionary.io;

import com.tomiche.exceptions.DictionaryDoesNotExist;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public class DictionaryFromFileReader {
    public String readFile(String fileName) throws DictionaryDoesNotExist {
        String fileContent;
        try {
            fileContent = extractFileContent(fileName);
        } catch (IOException e) {
            log.error(e.toString());
            throw new DictionaryDoesNotExist();
        }
        return fileContent;
    }

    private String extractFileContent(String path) throws IOException {
        return Files.readString(Paths.get("src", "main", "resources", "dictionaries", path));
    }
}
