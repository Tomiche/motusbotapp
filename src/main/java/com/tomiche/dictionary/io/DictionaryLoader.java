package com.tomiche.dictionary.io;

import com.tomiche.dictionary.Dictionary;
import com.tomiche.exceptions.DictionaryDoesNotExist;

public class DictionaryLoader {

    private Dictionary dictionary;

    public void loadDictionaryFromFile(String fileName) throws DictionaryDoesNotExist {
        if (null == fileName || fileName.isEmpty())
            throw new DictionaryDoesNotExist();
        String fileContent = new DictionaryFromFileReader().readFile(fileName);
        this.dictionary = new Dictionary(fileContent);
    }

    public Dictionary getLoadedDictionary() {
        return this.dictionary;
    }

    public void loadDictionaryOfWordsWithThisSpecificNumberOfLetters(int i) throws DictionaryDoesNotExist {
        loadDictionaryFromFile(String.valueOf(i) + "letters.txt");
    }
}
