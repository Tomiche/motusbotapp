package com.tomiche.dictionary;

import java.util.*;
import java.util.stream.Collectors;

public class Dictionary {
    public static final String WORD_SEPARATOR = " ";
    private final ArrayList<String> words;
    private LetterFinder letterFinder;
    private LettersFinder lettersFinder;
    private OrderedLettersFinder orderedLettersFinder;
    private LettersEliminatedOrNotWellPlacedAndOKFinder lettersEliminatedOrNotWellPlacedAndOKFinder;
    
    public Dictionary(String fileContent) {
        this.words = new ArrayList<>();
        init(fileContent);
    }

    private void init(String fileContent) {
        if (null != fileContent && !fileContent.isBlank()) {
            words.addAll(splitStringIntoWords(fileContent).parallelStream().map(word -> word.toUpperCase(Locale.ROOT)).collect(Collectors.toSet()));
            letterFinder = new LetterFinder(words);
            lettersFinder = new LettersFinder(letterFinder);
            orderedLettersFinder = new OrderedLettersFinder(words, letterFinder);
            lettersEliminatedOrNotWellPlacedAndOKFinder = new LettersEliminatedOrNotWellPlacedAndOKFinder(orderedLettersFinder);
        }
    }

    private Collection<String> splitStringIntoWords(String fileContent) {
        return Set.of(fileContent.split(WORD_SEPARATOR));
    }

    public List<String> getWords() {
        return words;
    }

    public Optional<List<String>> getWordsContainingThisLetter(char letter) {
        return letterFinder.find(letter);
    }

    public Optional<List<String>> getWordsContainingTheseLetters(List<Character> listOfLetters) {
        return lettersFinder.find(listOfLetters);
    }

    public Optional<List<String>> getWordsContainingTheseLettersInThisOrder(List<String> orderedLetters) {
        return orderedLettersFinder.find(orderedLetters);
    }


    public Optional<List<String>> getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(List<String> orderedLettersOK, List<String> lettersNotWellPlaced, List<String> lettersNotInTheWord) {
        return lettersEliminatedOrNotWellPlacedAndOKFinder.find(orderedLettersOK, lettersNotWellPlaced, lettersNotInTheWord);
    }
}
