package com.tomiche.dictionary.io;

import com.tomiche.exceptions.DictionaryDoesNotExist;
import com.tomiche.dictionary.io.DictionaryLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class TestDictionaryLoader {
    DictionaryLoader dictionaryLoader;

    @BeforeEach
    void setUp() {
        this.dictionaryLoader = new DictionaryLoader();
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"1letter.txt"})
    void loadMethod_shouldFailWhenNullFileName() {
        //loadMethod_shouldFailWhenNullFileName
        //loadMethod_shouldFailWhenEmptyFileName
        //loadMethod_shouldFailWhenFileNameDoesNotExist
        Assertions.assertThrows(DictionaryDoesNotExist.class, () -> {
            // Code under test
            this.dictionaryLoader.loadDictionaryFromFile("");
        });

        assertNull(this.dictionaryLoader.getLoadedDictionary());
    }

    @Test
    void loadMethod_shouldSucceedWhenFileNameExists() throws DictionaryDoesNotExist {
        this.dictionaryLoader.loadDictionaryFromFile("2letters.txt");
        assertNotNull(this.dictionaryLoader.getLoadedDictionary());
    }

    @Test
    void loadMethod_shouldSucceedWhenFileNameExistsAndFillDictionary() throws DictionaryDoesNotExist {
        this.dictionaryLoader.loadDictionaryFromFile("2letters.txt");
        assertNotNull(this.dictionaryLoader.getLoadedDictionary());
        assertNotNull(this.dictionaryLoader.getLoadedDictionary().getWords());
        assertFalse(this.dictionaryLoader.getLoadedDictionary().getWords().isEmpty());
    }

    @Test
    void loadDictionaryFromNotExistingNumberOfLetter_shouldThrowException() {
        Assertions.assertThrows(DictionaryDoesNotExist.class, () -> {
            // Code under test
            this.dictionaryLoader.loadDictionaryOfWordsWithThisSpecificNumberOfLetters(1);
        });
    }

    @Test
    void loadDictionaryFromAnExistingNumberOfLetter_shouldSucceed() throws DictionaryDoesNotExist {
        this.dictionaryLoader.loadDictionaryOfWordsWithThisSpecificNumberOfLetters(2);
        assertNotNull(this.dictionaryLoader.getLoadedDictionary());
        assertNotNull(this.dictionaryLoader.getLoadedDictionary().getWords());
        assertFalse(this.dictionaryLoader.getLoadedDictionary().getWords().isEmpty());
    }
}