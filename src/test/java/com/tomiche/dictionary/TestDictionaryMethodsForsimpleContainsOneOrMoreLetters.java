package com.tomiche.dictionary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestDictionaryMethodsForsimpleContainsOneOrMoreLetters {
    @Test
    void getWordsContainingThisLetter_shouldReturnAnEmptyListWhenUsingSpaceChar() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertTrue(dictionary.getWordsContainingThisLetter(' ').isEmpty());
    }

    @Test
    void getWordsContainingThisLetter_shouldReturnAnEmptyListWhenNoSuchWord() {
        Dictionary dictionary = new Dictionary("toto tata");
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingThisLetter('i');
        assertTrue(optionalResultList.isEmpty());
    }

    @Test
    void getWordsContainingThisLetter_shouldReturnANotEmptyListWhenMatchingWord() {
        Dictionary dictionary = new Dictionary("toto tata");
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingThisLetter('o');
        assertFalse(optionalResultList.isEmpty());
        Collection<String> matchingWordsList = optionalResultList.get();
        assertFalse(matchingWordsList.isEmpty());
        assertEquals(1, matchingWordsList.size());
        assertTrue(matchingWordsList.contains("TOTO"));
    }

    @Test
    void getWordsContainingThisLetter_shouldReturnANotEmptyListWhenMatchingWordEvenWithDifferentCase() {
        Dictionary dictionary = new Dictionary("TOTO TATA");
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingThisLetter('o');
        assertFalse(optionalResultList.isEmpty());
        Collection<String> matchingWordsList = optionalResultList.get();
        assertFalse(matchingWordsList.isEmpty());
        assertEquals(1, matchingWordsList.size());
        assertTrue(matchingWordsList.contains("TOTO"));
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenNullListPassed() {
        Dictionary dictionary = new Dictionary("toto tata");
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(null);
        assertTrue(optionalResultList.isEmpty());
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenNoLettersPassed() {
        Dictionary dictionary = new Dictionary("toto tata");
        List<Character> listOfLetters = new ArrayList<>();
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertTrue(optionalResultList.isEmpty());
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenNoMatchingLettersPassed() {
        Dictionary dictionary = new Dictionary("toto tata");
        List<Character> listOfLetters = new ArrayList<>();
        listOfLetters.add('i');
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertTrue(optionalResultList.isEmpty());
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnANotEmptyListWhenMatchingLettersPassed() {
        Dictionary dictionary = new Dictionary("toto tata");
        List<Character> listOfLetters = new ArrayList<>();
        listOfLetters.add('a');
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertFalse(optionalResultList.isEmpty());
        List<String> matchingWordsList = optionalResultList.get();
        assertFalse(matchingWordsList.isEmpty());
        assertEquals(1, matchingWordsList.size());
        assertTrue(matchingWordsList.contains("TATA"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"i,a", "a,i", "a,o"})
    void getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenWordDoesNotContainTheFirstLetter(String chars) {
        //getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenWordDoesNotContainAllTheLetters
        //getWordsContainingTheseLetters_shouldReturnAnEmptyListWhenLettersMatchDifferentWordsButNotTheSame
        Dictionary dictionary = new Dictionary("toto tata");
        List<Character> listOfLetters = Arrays.asList(chars.charAt(0), chars.charAt(1));
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertTrue(optionalResultList.isEmpty());
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnA2ItemsListWhenPassingMatchingLetters() {
        Dictionary dictionary = new Dictionary("tito tati");
        List<Character> listOfLetters = new ArrayList<>();
        listOfLetters.add('t');
        listOfLetters.add('i');
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertFalse(optionalResultList.isEmpty());
        List<String> resultList = optionalResultList.get();
        assertTrue(resultList.contains("TITO"));
        assertTrue(resultList.contains("TATI"));
    }

    @Test
    void getWordsContainingTheseLetters_shouldReturnA2ItemsListWhenPassingMatchingLettersEvenWithDifferentCases() {
        Dictionary dictionary = new Dictionary("TITO TATI");
        List<Character> listOfLetters = new ArrayList<>();
        listOfLetters.add('t');
        listOfLetters.add('i');
        Optional<List<String>> optionalResultList = dictionary.getWordsContainingTheseLetters(listOfLetters);
        assertFalse(optionalResultList.isEmpty());
        List<String> resultList = optionalResultList.get();
        assertTrue(resultList.contains("TITO"));
        assertTrue(resultList.contains("TATI"));
    }
}
