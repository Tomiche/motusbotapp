package com.tomiche.dictionary;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class TestDictionary {

    @Test
    void shouldReturnAListOfStrings() {
        Dictionary dictionary = new Dictionary("");
        assertTrue(dictionary.getWords() instanceof List);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"   "})
    void createWithNullFileContent_shouldBeEmpty() {
        //createWithEmptyFileContent_shouldBeEmpty
        //createWithBlankSpaceOnlyFileContent_shouldBeEmpty
        Dictionary dictionary = new Dictionary(null);
        assertTrue(dictionary.getWords().isEmpty());
    }

    @Test
    void createWithCorrectString_shouldNotBeEmpty() {
        Dictionary dictionary = new Dictionary("toto");
        assertFalse(dictionary.getWords().isEmpty());
    }

    @Test
    void createWithCorrect2WordsString_shouldHaveASizeOf2() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertEquals(2, dictionary.getWords().size());
    }

    @Test
    void dictionaryWordsShouldBeUpperCase() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertEquals("TOTO", dictionary.getWords().get(0));
        assertEquals("TATA", dictionary.getWords().get(1));
    }
}