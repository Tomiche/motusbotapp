package com.tomiche.dictionary;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class TestGetWordsWithLettersInACertainOrder {
    private static Stream<Arguments> provideStringsForIsBlank() {
        return Stream.of(
                Arguments.of("TOTO TATA", Arrays.asList("t", "o", "*", "*"), 1),
                Arguments.of("TOTO TATA TOTI", Arrays.asList("t", "o", "*", "*"), 2),
                Arguments.of("TOTO TATA TOTI TOBI", Arrays.asList("t", "o", "*", "i"), 2)
        );
    }

    @Test
    void getWordsContainingThisLetterInThisOrder_shouldBeEmptyWhenNullListPassedAsArgument() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertTrue(dictionary.getWordsContainingTheseLettersInThisOrder(null).isEmpty());
    }

    @Test
    void getWordsContainingThisLetterInThisOrder_shouldBeEmptyWhenEmptyListPassedAsArgument() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertTrue(dictionary.getWordsContainingTheseLettersInThisOrder(new ArrayList<>()).isEmpty());
    }

    @Test
    void getWordsContainingThisLetterInThisOrder_shouldBeEmptyWhenNoMatchingElement() {
        Dictionary dictionary = new Dictionary("toto tata");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("z");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        Optional<List<String>> wordsMatchingTheLettersInTheRightOrder = dictionary.getWordsContainingTheseLettersInThisOrder(orderedLetters);
        assertTrue(wordsMatchingTheLettersInTheRightOrder.isEmpty());
    }
    @DisplayName("should return a not empty list of words when matching element(s) found (even non consecutively)")
    @ParameterizedTest
    @MethodSource("provideStringsForIsBlank")
    void getWordsContainingThisLetterInThisOrder_shouldNotBeEmptyWhenMatchingElement(String dictionaryContent, List<String> wordLetters, int ResultNumber) {
        Dictionary dictionary = new Dictionary(dictionaryContent);
        List<String> orderedLetters = wordLetters;
        Optional<List<String>> wordsMatchingTheLettersInTheRightOrder = dictionary.getWordsContainingTheseLettersInThisOrder(orderedLetters);
        assertFalse(wordsMatchingTheLettersInTheRightOrder.isEmpty());
        assertEquals(ResultNumber, wordsMatchingTheLettersInTheRightOrder.get().size());
    }
    @DisplayName("should return an empty list of words when the first letter match but not the others")
    @Test
    void getWordsContainingThisLetterInThisOrder_shouldBeEmptyFirstLetterMatchButNotTheOthers() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("o");
        orderedLetters.add("*");
        orderedLetters.add("a");
        Optional<List<String>> wordsMatchingTheLettersInTheRightOrder = dictionary.getWordsContainingTheseLettersInThisOrder(orderedLetters);
        assertTrue(wordsMatchingTheLettersInTheRightOrder.isEmpty());
    }
}
