package com.tomiche.dictionary;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TestGetWordsWithLettersEliminatedOrNotWellPlacedAndOK {
    @Test
    void find_shouldBeEmptyWhenNullListPassedAsArgument() {
        Dictionary dictionary = new Dictionary("toto tata");
        assertTrue(dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(null, null, null).isEmpty());
        assertTrue(dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(null, new ArrayList<>(), new ArrayList<>()).isEmpty());
        assertTrue(dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(new ArrayList<>(), null, new ArrayList<>()).isEmpty());
        assertTrue(dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(new ArrayList<>(), new ArrayList<>(), null).isEmpty());
        assertTrue(dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(null, new ArrayList<>(), new ArrayList<>()).isEmpty());
    }

    @Test
    void find_shouldReturnAllTheWordsThatMatchesTheListOfOkLettersIfTheOtherListAreEmpty() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, new ArrayList<>(), new ArrayList<>());
        assertFalse(wordsMatching.isEmpty());
        assertEquals(4, wordsMatching.get().size());
    }

    @Test
    void find_shouldReturnAllTheWordsIfListOfLettersNotInTheWordDoesNotContainLettersPresentInTheDictionaryWords() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        List<String> lettersNotInTheWord = new ArrayList<>();
        lettersNotInTheWord.add("r");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, new ArrayList<>(), lettersNotInTheWord);
        assertFalse(wordsMatching.isEmpty());
        assertEquals(4, wordsMatching.get().size());
    }

    @Test
    void find_shouldReturnAnEmptyListIfLettersDoNotMatchTheDictionaryWords() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("r");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");

        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, new ArrayList<>(), new ArrayList<>());
        assertTrue(wordsMatching.isEmpty());
    }

    @Test
    void find_shouldReturnASubsetOfAllTheWordsIfListOfLettersNotInTheWordContainsLettersPresentInTheDictionaryWords() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        List<String> lettersNotInTheWord = new ArrayList<>();
        lettersNotInTheWord.add("i");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, new ArrayList<>(), lettersNotInTheWord);
        assertFalse(wordsMatching.isEmpty());
        assertEquals(2, wordsMatching.get().size());
        assertTrue(wordsMatching.get().contains("TATA"));
        assertTrue(wordsMatching.get().contains("TOTO"));
    }

    @Test
    void find_shouldReturnEmptyIfListOfOrderedLettersIsEmpty() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        List<String> lettersNotInTheWord = new ArrayList<>();
        lettersNotInTheWord.add("o");
        lettersNotInTheWord.add("a");
        List<String> notWellPlacedLetters = new ArrayList<>();
        notWellPlacedLetters.add("i");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, notWellPlacedLetters, lettersNotInTheWord);
        assertTrue(wordsMatching.isEmpty());
    }

    @Test
    void find_shouldReturnEmptyIfListOfLettersNotWellPlacedDoesNotContainLettersPresentInTheDictionaryWords() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        List<String> notWellPlacedLetters = new ArrayList<>();
        notWellPlacedLetters.add("r");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, notWellPlacedLetters, new ArrayList<>());
        assertTrue(wordsMatching.isEmpty());
    }

    @Test
    void find_shouldReturnASubsetOfTheListOfLettersWhenMatching() {
        Dictionary dictionary = new Dictionary("TOTO TATA TOTI TOBI");
        List<String> orderedLetters = new ArrayList<>();
        orderedLetters.add("t");
        orderedLetters.add("*");
        orderedLetters.add("*");
        orderedLetters.add("*");
        List<String> notWellPlacedLetters = new ArrayList<>();
        notWellPlacedLetters.add("*");
        notWellPlacedLetters.add("i");
        notWellPlacedLetters.add("*");
        notWellPlacedLetters.add("*");
        Optional<List<String>> wordsMatching = dictionary.getWordsContainingTheseLettersEliminatedOrNotWellPlacedOrOK(orderedLetters, notWellPlacedLetters, new ArrayList<>());
        assertFalse(wordsMatching.isEmpty());
        assertEquals(2, wordsMatching.get().size());
        assertTrue(wordsMatching.get().contains("TOTI"));
        assertTrue(wordsMatching.get().contains("TOBI"));
    }

}
